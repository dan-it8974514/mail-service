class AuthController {
    constructor() {
        this.httpClient = new HttpClient();
    }

    async auth(phone) {
        const data = {
            phone
        }

        const result = await this.httpClient.post('/auth', data);

        return result;
    }

    async verify(phone, code) {
        const data = {
            phone,
            code
        }

        const result = await this.httpClient.post('/verify', data);

        console.log('Result', result);

        return result;
    }
}

const main = async () => {
    // console.log("Hello, Client!");

    const authController = new AuthController();

    const form = document.getElementById('form');
    let phone
    let code

    form.addEventListener('submit', async (event) => {
        event.preventDefault();

        phone = document.getElementById('phone').value;

        const result = await authController.auth(phone);

        console.log(result);
        code = result.code;

        document.getElementById('verify').click();
    })

    const verifyForm = document.getElementById('verify-form');

    verifyForm.addEventListener('submit', async (event) => {
        event.preventDefault();

        // const code = document.getElementById('code').value;
        console.log('Code', code);
        const result = await authController.verify(phone, code);

        localStorage.setItem('token', result.token);

        console.log(result);

        // window.location.href = 'mail.html';
    })

    document.getElementById('submit').click();
}

main()