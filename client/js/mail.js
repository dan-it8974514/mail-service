class MailController {
    constructor() {
        this.httpClient = new HttpClient();
    }

    async getMails(type = 'inbox') {
        const result = await this.httpClient.get(`/mails?type=${type}`, {}, {
            'Authorization': `Bearer ${localStorage.getItem('token')}`
        });

        return result;
    }

    async sendMail(to, subject, body) {
        const result = await this.httpClient.post('/mails', {
            to,
            subject,
            body
        }, {
            'Authorization': `Bearer ${localStorage.getItem('token')}`
        });

        return result;
    }
}

const main = async () => {

    const mailController = new MailController();

    let mails = await mailController.getMails();

    const mailList = document.getElementById('mail-list');
    const mailContent = document.getElementById('mail-content');

    const renderMails = () => {
        mailList.innerHTML = '';

        mails.forEach((mail) => {
            const mailElement = document.createElement('div');


            mailElement.addEventListener('click', async (event) => {
                event.preventDefault();

                mailContent.innerHTML = `<p>${mail.body}</p>`
            })

            mailElement.innerHTML = `
                <div class="mail" data-id="${mail.id}">
                    <div class="from">${mail.from}</div>
                    <div class="subject">${mail.subject}</div>
                </div>
            `;


            mailList.appendChild(mailElement);
        })
    }

    if (mailList) {
        renderMails();
    }


    const form = document.getElementById('send-form');

    if (form) {

        form.addEventListener('submit', async (event) => {
            event.preventDefault();

            const to = document.getElementById('to').value;
            const subject = document.getElementById('subject').value;
            const body = document.getElementById('body').value;

            const result = await mailController.sendMail(to, subject, body);

            alert('Mail sent');

            window.location.href = 'mail.html';
        })
    }

    const inbox = document.getElementById('inbox');
    const sent = document.getElementById('sent');

    if (inbox) {
        inbox.classList.add('active');

        inbox.addEventListener('click', async (event) => {
            event.preventDefault();

            mails = await mailController.getMails();

            renderMails();

            sent.classList.remove('active');
            inbox.classList.add('active');
        })
    }

    if (sent) {
        sent.addEventListener('click', async (event) => {
            event.preventDefault();

            mails = await mailController.getMails('sent');

            renderMails();

            inbox.classList.remove('active');
            sent.classList.add('active');
        })
    }

    const socket = io('http://127.0.0.1:3002');

    socket.on('connect', () => {
        console.log('connected');
    })

    socket.on('mail', async (mail) => {
        mails = await mailController.getMails();

        renderMails();
    })

}

main()