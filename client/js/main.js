class HttpClient {
    constructor() {
        this.baseUrl = 'http://127.0.0.1:3001'
    }

    async get(url, query, headers = {}) {
        const urlencoded = new URLSearchParams(query).toString();
        console.log(urlencoded);
        const response = await fetch(`${this.baseUrl}${url}${urlencoded}`, {
            headers: {
                ...headers
            }
        });
        const data = await response.json();
        return data;
    }

    async post(url, data, headers = {}) {
        const response = await fetch(`${this.baseUrl}${url}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                ...headers
            },
            body: JSON.stringify(data)
        });
        const result = await response.json();
        return result;
    }
}