const { Server } = require('socket.io');

const corsOptions = {
    origin: 'http://127.0.0.1:3000',
}

const io = new Server({
    cors: corsOptions
});

module.exports = io