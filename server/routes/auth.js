const { Router } = require('express');
const jwt = require('jsonwebtoken')
const knex = require('../knex');
const router = Router();
const redisClient = require('../redis');

const getVerificationCode = () => {
    return Math.floor(1000 + Math.random() * 9000);
}

const AUTH_SECRET_KEY = 's3cr3t-k3y';

class JwtAuthService {
    sign(payload) {
        return jwt.sign(payload, AUTH_SECRET_KEY, { expiresIn: '1h' });
    }

    async verify(bearer) {
        const token = bearer.split(' ')[1];

        return new Promise(
            (resolve, reject) => jwt.verify(token, AUTH_SECRET_KEY, async (err, decoded) => {

                if (err) {
                    reject(null);
                } else {
                    const user = await knex('users').where({ phone: decoded.phone }).first();

                    resolve(user);
                }
            })
        )
    }
}

const authService = new JwtAuthService();

router.post('/auth', (req, res) => {
    const { phone } = req.body;

    const verificationCode = getVerificationCode();

    redisClient.set(phone, verificationCode, 'EX', 120);

    res.json({
        message: 'Verification code sent to your phone',
        code: verificationCode
    })
})

router.post('/verify', async (req, res) => {
    const { phone, code } = req.body;

    const storedCode = await redisClient.get(phone);

    if (Number(storedCode) === Number(code)) {
        // is user exist

        let user = await knex('users').where({ phone }).first();

        if (!user) {
            await knex('users').insert({
                phone
            })

            user = await knex('users').where({ phone }).first();
        }

        const token = authService.sign({ phone: user.phone });

        return res.json({
            verified: true,
            user,
            token,
            message: 'Verification successful'
        })
    }

    res.json({
        verified: false,
        message: 'Verification failed'
    })
})

const checkAuth = async (req, res, next) => {
    const token = req.headers['authorization'];

    if (!token) {
        return res.status(401).json({
            message: 'Unauthorized'
        })
    }

    const user = await authService.verify(token).catch(console.log)

    if (!user) {
        return res.status(401).json({
            message: 'Unauthorized'
        })
    }

    req.user = user;

    next();
}

module.exports = { router, checkAuth }