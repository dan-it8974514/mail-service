const { Router } = require('express');
const knex = require('../knex');
const io = require('../io');
// const { checkAuth } = require('./auth');

const router = Router();

router.get('/mails', async (req, res) => {
    const { type } = req.query;

    const mails = await knex('mails').select().where({ [type === 'inbox' ? 'to' : 'from']: req.user.phone });

    if (!mails) {
        return res.status(404).json({
            message: 'No mails found'
        })
    }

    res.json(mails);
})

router.post('/mails', async (req, res) => {
    try {
        const { to, subject, body } = req.body;

        await knex('mails').insert({
            from: req.user.phone,
            to,
            subject,
            body
        })

        io.emit('mail', { from: req.user.phone, to, subject, body });

        res.json({ message: 'Mail sent' });
    } catch (err) {
        console.log(err);
        res.status(500).json({ message: 'Internal server error' });
    }
})

module.exports = { router }
