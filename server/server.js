const express = require("express");
const bodyParser = require("body-parser");
const morgan = require("morgan");
const cors = require("cors");
const redisClient = require('./redis');
const { checkAuth, router: authRouter } = require('./routes/auth');
const { router: mailsRouter } = require('./routes/mails');
const io = require('./io');
const corsOptions = {
    origin: 'http://127.0.0.1:3000',
}

const app = express();

app.use(bodyParser.json());
app.use(morgan("dev"));
app.use(cors(corsOptions));

app.use('/', authRouter);
app.use('/', checkAuth, mailsRouter);

app.get("/", (req, res) => {
    res.send("Hello, Server!");
})

io.on('connection', (socket) => {
    console.log('a user connected');
    socket.on('disconnect', () => {
        console.log('user disconnected');
    });
})

app.listen(3001, async () => {


    io.listen(3002);

    console.log("Server is running on port 3001");
    console.log("Socket.io is running on port 3002");

    // await knex.schema.createTable('users', table => {
    //     table.increments('id');
    //     table.string('phone');
    //     table.timestamp('created_at').defaultTo(knex.fn.now());
    //     table.timestamp('updated_at').defaultTo(knex.fn.now());

    //     table.unique('phone');
    // })

    // await knex.schema.createTable('mails', table => {
    //     table.increments('id');
    //     table.string('from');
    //     table.string('to');
    //     table.string('subject');
    //     table.string('body');
    //     table.timestamp('created_at').defaultTo(knex.fn.now());
    //     table.timestamp('updated_at').defaultTo(knex.fn.now());    
    // })

    await redisClient.connect();
});